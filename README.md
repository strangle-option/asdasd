Some indicators lie within the ranges and these range bound indicators are known as oscillators. Oscillators are the most common indicators. Their values range between maxima and minima of 100 and zero. These ranges show periods of signals, for example when value is approaching 100 then security is overbought but if value is touching the zero, it is oversold. Non-bounded indicators also show signals for strangle option .selling and buying as well as display weaknesses and strengths but their mode of action is different than that of bounded indicators. Both types of indicators use two methods to detect selling and buying signals.

Crossovers

Divergences

Crossovers are more popular than divergences. Crossover are used when traverse of two different averages occurs or when moving averages crossovers the price. Divergence is considered when two trends or indicators head in opposite directions. Divergence informs us about the lowering or downward trend in price moves.

read more articles at: .https://www.investopedia.com/